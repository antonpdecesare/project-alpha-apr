from django.shortcuts import render, redirect
from projects.models import Project
from projects.forms import CreateProject
from django.contrib.auth.decorators import login_required


@login_required
def list_projects(request):
    projects = Project.objects.filter(owner=request.user)
    context = {"projects": projects}
    return render(request, "projects/list.html", context)


@login_required
def show_project(request, id):
    project = Project.objects.get(id=id)
    context = {
        "show_project": project,
    }
    return render(request, "projects/detail.html", context)


@login_required
def create_project(request):
    if request.method == "POST":
        form = CreateProject(request.POST)
        if form.is_valid():
            projects = form.save(commit=False)
            projects.owner = request.user
            projects.save()
            return redirect("list_projects")
    else:
        form = CreateProject()
    context = {"form": form}
    return render(request, "projects/create.html", context)
